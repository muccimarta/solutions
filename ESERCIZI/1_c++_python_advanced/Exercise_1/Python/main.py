def shape(n: int, m: int = 0):
    """
    shape   Shaped matrix.
        shape(n,m) is the n-by-m matrix with elements from 1 to n * m.
    """
    return None


def rand(n: int, m: int = 0):
    """
    rand   Random matrix.
        rand(n,m) is the n-by-m matrix with random elements.
    """
    return None


def hilb(n: int, m: int = 0):
    """
    hilb   Hilbert matrix.
       hilb(n,m) is the n-by-m matrix with elements 1/(i+j-1).
       it is a famous example of a badly conditioned matrix.
       cond(hilb(n)) grows like exp(3.5*n).
       hilb(n) is symmetric positive definite, totally positive, and a
       Hankel matrix.
    """
    return None


def solveSystem(A):
    """
    solveSystem   Solve system with matrix.
       solveSystem(A) solve the linear system Ax=b with x ones.
       returns the determinant, the condition number and the relative error
    """
    return 0., 0., 0.


if __name__ == '__main__':
    n: int = 2

    [detAS, condAS, errRelS] = solveSystem(shape(n, n))
    print("{:} - DetA: {:.4e}, RCondA: {:.4e}, Relative Error: {:.4e}".format('shape', detAS, 1. / condAS, errRelS))
    [detAR, condAR, errRelR] = solveSystem(rand(n, n))
    print("{:} - DetA: {:.4e}, RCondA: {:.4e}, Relative Error: {:.4e}".format('rand', detAR, 1. / condAR, errRelR))
    [detAH, condAH, errRelH] = solveSystem(hilb(n, n))
    print("{:} - DetA: {:.4e}, RCondA: {:.4e}, Relative Error: {:.4e}".format('hilb', detAH, 1. / condAH, errRelH))


    exit(0)
