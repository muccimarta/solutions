# Linear Algebra Systems Solve

For Computational Science solving linear systems if fundamental for every application

## Example 
The solution of `Ax = b` with

`A = [1, 5; 0, 18]`

`b = [1; 23]`

using PALU decomposition is:

Ly = Pb

Ux = y

`x = [1;1]`

## Requirements

Write a software able to compute the linear system solution with PALU decomposition of the following systems:

1. `A` is `n` x `n` squared matrix with elements from `1` to `n^2`
2. `A` is `n` x `n`  squared matrix with random elements
3. `A` is `n` x `n`  hilbert squared matrix
   `A[i, j] = 1 / (1 + i + j)`

and the solution is always the unitary vector.
