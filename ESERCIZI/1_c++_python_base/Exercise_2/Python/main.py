#!/usr/bin/env python3
import sys



# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    try:
        text_file = open(inputFilePath,"r")
        testo = text_file.read()
        text_file.close()
        return True, testo 
    except:
        return False, ""


# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    try:
        encryptedText=""
        for i in range(0,len(text)):
            p = password[i % len(password)]
            c = text[i]
            num_p=ord(p)  #trasforma i caratteri in numeri così da poter sommare la loro posizione
            num_c=ord(c)
            num_A=ord('A')
            sum = num_c + num_p-num_A +1
            encryptedText = encryptedText + chr(sum)  #ritrasformo sum da numero a carattere
            
        return True, encryptedText
  
    except:
        return False, ""
  

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    try:
        decryptedText=""
        for i in range(0,len(text)):
            p = password[i % len(password)]
            c = text[i]
            num_p=ord(p)  #trasforma i caratteri in numeri così da poter sommare la loro posizione
            num_c=ord(c)
            num_A=ord('A')
            sum = num_c - (num_p-num_A +1)
            decryptedText = decryptedText + chr(sum)  #ritrasformo sum da numero a carattere
        return True, decryptedText
    except:
        return False, ""

    


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
