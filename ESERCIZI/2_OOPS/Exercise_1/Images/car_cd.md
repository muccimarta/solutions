```plantuml
@startuml

class CarFactory {
  +{static} void StartProduction(CarProducer producer)
  +{static} Car Create(string color)
}
note left of CarFactory: StartProduction()\n  starts the factory production
note bottom of CarFactory: Create()\n  creates an instance of a car

class Car {
  +Car(string producer, string model, string color)
  +string Show()
}
note left of Car: Show()\n  returns the color, producer\n  and model of the car

enum CarProducer {
  UNKNOWN = 0
  FORD = 1
  TOYOTA = 2
  VOLKSWAGEN = 3
}

CarProducer --o  CarFactory : -__producer__
Car "*" <-- "1" CarFactory : creates 

@enduml
```