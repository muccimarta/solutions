from enum import Enum


class Car:
    def __init__(self, producer: str, model: str, color: str):
        pass

    def show(self) -> str:
        return ""


class CarProducer(Enum):
    UNKNOWN = 0
    FORD = 1
    TOYOTA = 2
    VOLKSWAGEN = 3


class CarFactory:
    __producer = CarProducer.UNKNOWN

    @staticmethod
    def start_production(producer: CarProducer):
        pass

    @staticmethod
    def create(color: str) -> Car:
        return None
