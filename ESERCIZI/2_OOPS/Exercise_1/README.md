# Car Factory

On a car brochure, a car is identified by the following properties:

* Color
* Producer
* Model

## Example 

See the image below as an example of three different cars

![car](Images/car.png)

## Requirements

Write a software which emulate a car factory who produces a car on the brochure.

### Basic

The factory is able to create a car with the following structure:

![car_cd](Images/car_cd.png)

The format required of function `Show()` of class `Car` is the following:
```c++
model + " (" + producer + "): color " + color
```
**Example**
```c++
"Mustang (Ford): color Red"
```

### Advance

Try to extend the software applying the abstraction and polymorphism OOP concepts using the following structure:

![car_cd2](Images/car_cd2.png)