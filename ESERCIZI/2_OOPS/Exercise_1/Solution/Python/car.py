from enum import Enum


class Car:
    def __init__(self, producer: str, model: str, color: str):
        self.__color = color
        self.__producer = producer
        self.__model = model

    def show(self) -> str:
        return self.__model + " (" + self.__producer + "): color " + self.__color


class CarProducer(Enum):
    UNKNOWN = 0
    FORD = 1
    TOYOTA = 2
    VOLKSWAGEN = 3


class CarFactory:
    __producer = CarProducer.UNKNOWN

    @staticmethod
    def start_production(producer: CarProducer):
        CarFactory.__producer = producer

    @staticmethod
    def create(color: str) -> Car:
        if CarFactory.__producer == CarProducer.FORD:
            return Car("Ford", "Mustang", color)
        elif CarFactory.__producer == CarProducer.TOYOTA:
            return Car("Toyota", "Prius", color)
        elif CarFactory.__producer == CarProducer.VOLKSWAGEN:
            return Car("Volkswagen", "Golf", color)
        else:
            raise ValueError("Invalid producer " + str(CarFactory.__producer))
