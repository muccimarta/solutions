#include "shape.h"
#include <math.h>

namespace ShapeLibrary
{
    double Ellipse::Area() const { return a * b * M_PI; }
    double Point::dist(const Point &point) const
    {
        double dif_x = x - point.x;
        double dif_y = y - point.y;
        return sqrt(dif_x * dif_x + dif_y * dif_y);
    }
    double Triangle::Area() const
    {
        double a = p1.dist(p2);
        double b = p2.dist(p3);
        double c = p3.dist(p1);
        double semi_p = (a + b + c) / 2;
        return sqrt(semi_p * (semi_p - a) * (semi_p - b) * (semi_p - c));
    }
}
