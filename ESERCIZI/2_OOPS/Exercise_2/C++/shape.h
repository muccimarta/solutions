#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>


using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double x;
      double y;
      Point(const double& x,
            const double& y): x(x), y(y) { }
      Point(const Point& point) = default;
      double dist(const Point& point)const;
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  { private:
    Point center;
    int a;
    int b;
    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b) : center(center), a(a), b(b) { }

      double Area() const;
      
  };

  class Circle : public Ellipse
  { 
    public:
      Circle(const Point& center,
             const int& radius) : Ellipse(center, radius, radius){ }
  };


  class Triangle : public IPolygon
  {private:
    Point p1;
    Point p2;
    Point p3;
    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3) : p1(p1), p2(p2), p3(p3) { }

      double Area() const;
  };


  class TriangleEquilateral : public IPolygon
  { private:
    Point p1;
    double edge;
    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge): p1(p1), edge(edge) { }

      double Area() const { return edge*edge*0.4330127018922193; }
  };

  class Quadrilateral : public IPolygon
  {private:
    Point p1;
    Point p2;
    Point p3;
    Point p4;
    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4): p1(p1), p2(p2), p3(p3), p4(p4) { }

      double Area() const { 
        return Triangle(p1,p2,p4).Area() + Triangle(p2,p3,p4).Area(); }
  };


  class Parallelogram : public IPolygon
  {private:
    Point p1;
    Point p2;
    Point p4;
    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4) : p1(p1), p2(p2), p4(p4){ }

      double Area() const { return 2*Triangle(p1,p2,p4).Area(); }
  };

  class Rectangle : public IPolygon
  {private:
    Point p1;
    int base;
    int height;
    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height): p1(p1), base(base), height(height) { }

      double Area() const { return base*height; }
  };

  class Square: public Rectangle
  {
    public:
      Square(const Point& p1,
             const int& edge): Rectangle(p1, edge, edge) { }

    
  };
}

#endif // SHAPE_H
