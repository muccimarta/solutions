import math
class Point:
    def __init__(self, x: float, y: float):
        self.x=x
        self.y=y
    def dist(self, p):
       return math.sqrt((self.x - p.x)**2 +(self.y - p.y)**2)
        



class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.center = center
        self.a=a
        self.b=b
    def area(self):
        return self.a * self.b * math.pi 


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super().__init__(center,radius,radius)
    

class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.p1=p1
        self.p2=p2
        self.p3=p3
    def area(self):
        a = self.p1.dist(self.p2)
        b = self.p2.dist(self.p3)
        c = self.p3.dist(self.p1)
        semi_p = (a+b+c)/2
        return math.sqrt(semi_p*(semi_p-a)*(semi_p-b)*(semi_p-c))


class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.edge = edge
    def area(self):
        return self.edge**2*math.sqrt(3)/4


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.p1=p1
        self.p2=p2
        self.p3=p3
        self.p4=p4
    def area(self):
        return Triangle(self.p1,self.p2,self.p3).area() + Triangle(self.p3,self.p4,self.p4)
        



class Parallelogram(IPolygon):
    def __init__(self, p1: Point, p2: Point, p4: Point):
         self.p1=p1
         self.p2=p2
         self.p4=p4
    def area(self):
        return Triangle(self.p1,self.p2,self.p4).area() * 2



    


class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        self.base=base
        self.height=height
    def area(self):
        return self.base * self.height


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, edge, edge)
