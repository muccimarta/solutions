#ifndef SHOPPING_H
#define SHOPPING_H

#include <iostream>

using namespace std;

namespace ShoppingLibrary {

  class IShoppingApp {
    public:
      virtual unsigned int NumberElements() const = 0;
      virtual void AddElement(const string& product) = 0;
      virtual void Undo() = 0;
      virtual void Reset() = 0;
      virtual bool SearchElement(const string& product) = 0;
  };

  class VectorShoppingApp : public IShoppingApp {
    public:
      unsigned int NumberElements() const { throw runtime_error("Unimplemented Method"); }
      void AddElement(const string& product) { throw runtime_error("Unimplemented Method"); }
      void Undo() { throw runtime_error("Unimplemented Method"); }
      void Reset() { throw runtime_error("Unimplemented Method"); }
      bool SearchElement(const string& product) { throw runtime_error("Unimplemented Method"); }
  };

  class ListShoppingApp : public IShoppingApp {
    public:
      unsigned int NumberElements() const { throw runtime_error("Unimplemented Method"); }
      void AddElement(const string& product) { throw runtime_error("Unimplemented Method"); }
      void Undo() { throw runtime_error("Unimplemented Method"); }
      void Reset() { throw runtime_error("Unimplemented Method"); }
      bool SearchElement(const string& product) { throw runtime_error("Unimplemented Method"); }
  };

  class QueueShoppingApp : public IShoppingApp {
    public:
      unsigned int NumberElements() const { throw runtime_error("Unimplemented Method"); }
      void AddElement(const string& product) { throw runtime_error("Unimplemented Method"); }
      void Undo() { throw runtime_error("Unimplemented Method"); }
      void Reset() { throw runtime_error("Unimplemented Method"); }
      bool SearchElement(const string& product) { throw runtime_error("Unimplemented Method"); }
  };

  class StackShoppingApp : public IShoppingApp {
    public:
      unsigned int NumberElements() const { throw runtime_error("Unimplemented Method"); }
      void AddElement(const string& product) { throw runtime_error("Unimplemented Method"); }
      void Undo() { throw runtime_error("Unimplemented Method"); }
      void Reset() { throw runtime_error("Unimplemented Method"); }
      bool SearchElement(const string& product) { throw runtime_error("Unimplemented Method"); }
  };

  class HashShoppingApp : public IShoppingApp {
    public:
      unsigned int NumberElements() const { throw runtime_error("Unimplemented Method"); }
      void AddElement(const string& product) { throw runtime_error("Unimplemented Method"); }
      void Undo() { throw runtime_error("Unimplemented Method"); }
      void Reset() { throw runtime_error("Unimplemented Method"); }
      bool SearchElement(const string& product) { throw runtime_error("Unimplemented Method"); }
  };
}

#endif // SHOPPING_H
