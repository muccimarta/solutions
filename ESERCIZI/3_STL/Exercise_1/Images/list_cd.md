```plantuml
@startuml

interface IShoppingApp {
  +uint NumberElements()
  +void AddElement(string product)
  +void Undo()
  +void Reset()
  +bool SearchElement(string product)
}
note left of IShoppingApp: AddElement()\n  add an element
note top of IShoppingApp: Undo()\n  remove last element inserted
note left of IShoppingApp: Reset()\n  remove all the elements
note right of IShoppingApp: SearchElement()\n  check if the element exists

class VectorShoppingApp
class ListShoppingApp
class StackShoppingApp
class QueueShoppingApp
class HashShoppingApp

IShoppingApp <|.. VectorShoppingApp
IShoppingApp <|.. ListShoppingApp
IShoppingApp <|.. StackShoppingApp
IShoppingApp <|.. QueueShoppingApp
IShoppingApp <|.. HashShoppingApp

@enduml
```