#ifndef SHOPPING_H
#define SHOPPING_H

#include <iostream>
#include <vector>
#include <list>
#include <stack>
#include <queue>
#include <unordered_set>

using namespace std;

namespace ShoppingLibrary {

  class IShoppingApp {
    public:
      virtual unsigned int NumberElements() const = 0;
      virtual void AddElement(const string& product) = 0;
      virtual void Undo() = 0;
      virtual void Reset() = 0;
      virtual bool SearchElement(const string& product) = 0;
  };

  class VectorShoppingApp : public IShoppingApp {
    private:
      vector<string> elements;
    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push_back(product); }
      void Undo() { elements.pop_back(); }
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product)
      {
        // return find(elements.begin(), elements.end(), product) != elements.end()

        unsigned int numElements = NumberElements();
        for (unsigned int i = 0; i < numElements; i++)
        {
          if (elements[i] == product)
            return true;
        }

        return false;
      }
  };

  class ListShoppingApp : public IShoppingApp {
    private:
      list<string> elements;
    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push_back(product); }
      void Undo() { elements.pop_back(); }
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product)
      {
        // return find(elements.begin(), elements.end(), product) != elements.end()

        for(list<string>::iterator it = elements.begin(); it != elements.end(); ++it)
        {
          if (*it == product)
            return true;
        }

        return false;
      }
  };

  class StackShoppingApp : public IShoppingApp {
    private:
      stack<string> elements;
    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push(product); }
      void Undo() { elements.pop(); }
      void Reset() { unsigned int numElements = NumberElements();  for (unsigned int i = 0; i < numElements; i++) elements.pop(); }
      bool SearchElement(const string& product)
      {
        bool found = false;
        stack<string> searchStack;
        unsigned int numElements = NumberElements();
        for (unsigned int i = 0; i < numElements; i++)
        {
          string element = elements.top();
          searchStack.push(element);
          elements.pop();
          if (element == product)
          {
            found = true;
            break;
          }
        }

        unsigned int numSearchElements = searchStack.size();
        for (unsigned int i = 0; i < numSearchElements; i++)
        {
          elements.push(searchStack.top());
          searchStack.pop();
        }

        return found;
      }
  };

  class QueueShoppingApp : public IShoppingApp {
    private:
      queue<string> elements;
    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push(product); }
      void Undo() {
        queue<string> undoQueue;
        unsigned int numElements = NumberElements();
        for (unsigned int i = 0; i < numElements - 1; i++)
        {
          undoQueue.push(elements.back());
          elements.pop();
        }

        elements.pop();

        unsigned int numSearchElements = undoQueue.size();
        for (unsigned int i = 0; i < numSearchElements; i++)
        {
          elements.push(undoQueue.front());
          undoQueue.pop();
        }
      }
      void Reset() { unsigned int numElements = NumberElements();  for (unsigned int i = 0; i < numElements; i++) elements.pop(); }
      bool SearchElement(const string& product)
      {
        bool found = false;
        queue<string> searchQueue;
        unsigned int numElements = NumberElements();
        for (unsigned int i = 0; i < numElements; i++)
        {
          string element = elements.back();
          searchQueue.push(element);
          elements.pop();
          if (element == product)
          {
            found = true;
            break;
          }
        }

        unsigned int numSearchElements = searchQueue.size();
        for (unsigned int i = 0; i < numSearchElements; i++)
        {
          elements.push(searchQueue.front());
          searchQueue.pop();
        }

        return found;
      }
  };

  class HashShoppingApp : public IShoppingApp {
    private:
      string lastElement;
      unordered_set<string> elements;
    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { lastElement = product; elements.insert(product); }
      void Undo() { elements.erase(lastElement); }
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product) { return elements.find(product) != elements.end();}
  };
}

#endif // SHOPPING_H
