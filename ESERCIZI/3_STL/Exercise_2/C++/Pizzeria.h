#ifndef PIZZERIA
#define PIZZERIA

#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>

using namespace std;

namespace PizzeriaLibrary
{

  class Ingredient
  {
  public:
    string Name;
    int Price;
    string Description;
    Ingredient(const string &name, int price, const string &description) : Name(name), Price(price), Description(description) {}
  };

  class Pizza
  {

  public:
    string Name;
    vector<Ingredient> ListaIngredienti;

    Pizza(const string &name) : Name(name) {}
    Pizza(const string &name, const vector<Ingredient> &listaIngredienti) : Name(name), ListaIngredienti(listaIngredienti) {}
    void AddIngredient(const Ingredient &ingredient)
    {
      for (auto &ing : ListaIngredienti)
      {
        if (ing.Name == ingredient.Name)
          throw runtime_error("Ingredient already inserted");
      }
      ListaIngredienti.push_back(ingredient);
    }
    int NumIngredients() const
    {
      int num = ListaIngredienti.size();
      return num;
    }
    int ComputePrice() const
    {
      int sum = 0;
      for (auto &ing : ListaIngredienti)
      {
        sum = sum + ing.Price;
      }
      return sum;
    }
  };

  class Order
  {
  public:
    vector<Pizza> ListaPizze;
    int numPizzas;
    Order(const vector<Pizza> &listaPizze) : ListaPizze(listaPizze), numPizzas(ListaPizze.size()) {}
    Order() {}
    void InitializeOrder(int numPizzas)
    {
      this->numPizzas = numPizzas;
    }
    void AddPizza(const Pizza &pizza)
    {
      ListaPizze.push_back(pizza);
    }
    const Pizza &GetPizza(const int &position) const
    {
      if (position < 1 || position > numPizzas || position > ListaPizze.size())
        throw runtime_error("Position passed is wrong");
      return ListaPizze[position - 1];
    }
    int NumPizzas() const
    {
      return numPizzas;
    }
    int ComputeTotal() const
    {
      int sum = 0;
      for (auto &pizza : ListaPizze)
      {
        sum = sum + pizza.ComputePrice();
      }
      return sum;
    }
  };

  class Pizzeria
  {
  public:
    vector<Pizza> ListaPizze;
    vector<Order> ListaOrders;
    vector<Ingredient> ListaIngredients;
    void AddIngredient(const string &name,
                       const string &description,
                       const int &price)
    {
      for (auto &ing : ListaIngredients)
      {
        if (ing.Name == name)
          throw runtime_error("Ingredient already inserted");
      }
      ListaIngredients.emplace_back(name, price, description);
    }
    const Ingredient &FindIngredient(const string &name) const
    {
      for (auto &ing : ListaIngredients) // AUTO:usato per dichiarare il parametro del ciclo for
      {
        if (ing.Name == name)
          return ing;
      }
      throw runtime_error("Ingredient not found");
    }

    void AddPizza(const string &name,
                  const vector<string> &ingredients)
    {
      bool trovata = false;
      try
      {
        FindPizza(name);
        trovata = true;
      }
      catch (...)
      {
        vector<Ingredient> ingredienti;
        for (auto &i : ingredients)
        {
          ingredienti.push_back(FindIngredient(i));
        }
        ListaPizze.emplace_back(name, ingredienti);
      }
      if (trovata)
        throw runtime_error("Pizza already inserted");
    }

    const Pizza &FindPizza(const string &name) const
    {
      for (auto &pizza : ListaPizze)
      {
        if (pizza.Name == name)
          return pizza;
      }
      throw runtime_error("Pizza not found");
    }
    int CreateOrder(const vector<string> &pizzas)
    {
      if(pizzas.empty()){
        throw runtime_error("Empty order");
      }
      vector<Pizza> MorePizze;
      for (auto &p : pizzas)
      {
        MorePizze.push_back(FindPizza(p));
      }
      ListaOrders.emplace_back(MorePizze);
      return ListaOrders.size() - 1 + 1000;
    }
    const Order &FindOrder(const int &numOrder) const
    {
      try
      {
        return ListaOrders.at(numOrder - 1000);
      }
      catch (...)
      {
        throw runtime_error("Order not found");
      }
    }
    string GetReceipt(const int &numOrder) const
    {
      const Order &o = FindOrder(numOrder);
      ostringstream receipt;
      for (auto &p : o.ListaPizze)
      {
        receipt << "- " << p.Name << ", " << p.ComputePrice() << " euro" << endl;
      }
      receipt << "  TOTAL: " << o.ComputeTotal() << " euro" << endl;
      return receipt.str();
    }

    string ListIngredients() const
    {
      ostringstream ing;
      auto List2 = ListaIngredients;
      std::sort(List2.begin(), List2.end(), [](const Ingredient &a, const Ingredient &b)
                { return a.Name < b.Name; });
      for (auto &i : List2)
      {
        ing << i.Name << " - '" << i.Description << "': " << i.Price << " euro" << endl;
      }

      return ing.str();
    }

    string Menu() const
    {
      ostringstream pizza;
      std::vector<Pizza> lista2 = ListaPizze;
      std::sort(lista2.begin(), lista2.end(), [](const Pizza &a, const Pizza &b)
                { return a.Name > b.Name; });
      for (auto &p : lista2)
      {
        pizza << p.Name << " (" << p.NumIngredients() << " ingredients): " << p.ComputePrice() << " euro" << endl;
      }

      return pizza.str();
    }
  };
};

#endif /* PIZZERIA */
