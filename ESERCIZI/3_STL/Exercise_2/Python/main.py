from typing import List


class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


ingrList = List[Ingredient]


class Pizza:
    def __init__(self, name: str, ingredients: ingrList):
        self.Name = name
        self.IngredientsList = []
        for i in range(len(ingredients)):
            self.IngredientsList.append(ingredients[i])
            self.totIngredients = len(ingredients)
            self.Price = 0

    def addIngredient(self, ingredient: Ingredient):
        self.IngredientsList.append(ingredient)
        self.totIngredients = self.totIngredients + 1

    def numIngredients(self) -> int:
        return self.totIngredients

    def computePrice(self) -> int:
        TotalPrice: int = 0
        for i in range(self.totIngredients):
            TotalPrice += self.IngredientsList[i].Price
        return TotalPrice


pizzaList = List[Pizza]


class Order:
    def __init__(self, pizzas: pizzaList):
        self.PizzasList: pizzaList = []
        for i in range(len(pizzas)):
            self.PizzasList.append(pizzas[i])
        self.numPizzas: int = len(pizzas)
        self.amount = 0
    def getPizza(self, position: int) -> Pizza:
        if position in self.PizzasList:
            return self.PizzasList[position]
        else:
            raise Exception("Position passed is wrong")

    def initializeOrder(self, numPizzas: int):
        self.numPizzas=numPizzas

    def addPizza(self, pizza: Pizza):
        self.PizzasList.append(pizza)
        self.numPizzas=self.numPizzas+1

    def numPizzas(self) -> int:
        return self.numPizzas

    def computeTotal(self) -> int:
        TotPrice: int = 0
        for i in range(self.numPizzas):
            TotPrice += self.PizzasList[i].computePrice()
        return TotPrice


class Pizzeria:
    def __init__(self):
        self.IngredientsPizzeria = dict()
        self.PizzasPizzeria = dict()
        self.OrdersPizzeria = dict()

    def addIngredient(self, name: str, description: str, price: int):
        
        newIngredient = Ingredient(name, price, description)
        if newIngredient.Name in self.IngredientsPizzeria:
            raise Exception("Ingredient already inserted")
        else:
            self.IngredientsPizzeria.update({newIngredient.Name: newIngredient})

    def findIngredient(self, name: str) -> Ingredient:
        if name in self.IngredientsPizzeria:
            return self.IngredientsPizzeria[name]
        else:
            raise Exception("Ingredient not found")

    def addPizza(self, name: str, ingredientsName):
        ingredients: ingrList = []
        for i in range(len(ingredientsName)):
            ingredients.append(self.findIngredient(ingredientsName[i]))
        newPizza = Pizza(name, ingredients)
        newPizza.Price = newPizza.computePrice()
        if newPizza.Name in self.PizzasPizzeria:
            raise Exception("Pizza already inserted")
        else:
            self.PizzasPizzeria.update({newPizza.Name: newPizza})  

    def findPizza(self, name: str) -> Pizza:
        if name in self.PizzasPizzeria:
            return self.PizzasPizzeria[name]
        else:
            raise Exception("Pizza not found")

    def createOrder(self, pizzaNames) -> int:
        if not pizzaNames:
            raise Exception("Empty order")
        pizzas: pizzaList = []
        for i in range(len(pizzaNames)):
            pizzas.append(self.findPizza(pizzaNames[i]))
        numOrder: int = 1000 + len(self.OrdersPizzeria)
        order = Order(pizzas)
        order.amount = order.computeTotal()
        if len(pizzas) > 0:
            self.OrdersPizzeria.update({numOrder: order})
            return numOrder

    def findOrder(self, numOrder: int) -> Order:
        if numOrder in self.OrdersPizzeria:
            return self.OrdersPizzeria[numOrder]
        else:
            raise Exception("Order not found")

    def getReceipt(self, numOrder: int) -> str:
        output = ""
        if numOrder in self.OrdersPizzeria:
            order: Order = self.OrdersPizzeria.get(numOrder)
            length = order.numPizzas
            for i in range(length):
                output += "- " + str(order.PizzasList[i].Name) + ", " + str(
                    order.PizzasList[i].Price) + " euro" + "\n"
            output += "  TOTAL: " + str(order.amount) + " euro" + "\n"
            return output
        else:
            raise Exception("Order not found")

    def listIngredients(self) -> str:
        output = ""
        for name in sorted(self.IngredientsPizzeria.keys()):
            output += self.IngredientsPizzeria[name].Name + " - '" + self.IngredientsPizzeria[name].Description + "': " + str(self.IngredientsPizzeria[name].Price) + " euro" + "\n"
        return output

    def menu(self) -> str:
        output = ""
        for key in self.PizzasPizzeria:
            output += self.PizzasPizzeria[key].Name + " (" + str(self.PizzasPizzeria[key].numIngredients()) + " ingredients): " + str(self.PizzasPizzeria[key].computePrice()) + " euro" + "\n"
        return output

