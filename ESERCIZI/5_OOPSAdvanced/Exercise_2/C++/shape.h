#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary
{

  class Point
  {
  public:
    double X;
    double Y;
    Point() : X(0), Y(0) {}
    Point(const double &x,
          const double &y) : X(x), Y(y) {}
    Point(const Point &point) : X(point.X), Y(point.Y) {}

    double ComputeNorm2() const
    {
      return sqrt(X * X + Y * Y);
    }

    Point operator+(const Point &point) const
    {
      return Point(X + point.X, Y + point.Y);
    }
    Point operator-(const Point &point) const
    {
      return Point(X - point.X, Y - point.Y);
    }
    Point &operator-=(const Point &point)
    {
      X = X - point.X;
      Y = Y - point.Y;
      return *this;
    }
    Point &operator+=(const Point &point)
    {
      X = X + point.X;
      Y = Y + point.Y;
      return *this; //return *this will return the the value in the address stored by this
    }
    friend ostream &operator<<(ostream &stream, const Point &point)
    {
      stream << "Point: x = " << point.X << " y = " << point.Y << endl;
      return stream;
    }
  };

  class IPolygon
  {
  protected:
    vector<Point> points;

  public:
    virtual double Perimeter() const
    {
      double retVal = 0.0;
      for (int i = 0; i < points.size(); i++)
      {
        retVal += (points[i] - points[(i + 1) % points.size()]).ComputeNorm2();
      }
      return retVal;
    }
    virtual void AddVertex(const Point &point)
    {
      points.push_back(point);
    }
    friend inline bool operator<(const IPolygon &lhs, const IPolygon &rhs) { return lhs.Perimeter() < rhs.Perimeter(); }
    friend inline bool operator>(const IPolygon &lhs, const IPolygon &rhs) { return lhs.Perimeter() > rhs.Perimeter(); }
    friend inline bool operator<=(const IPolygon &lhs, const IPolygon &rhs) { return !operator>(lhs,rhs); }
    friend inline bool operator>=(const IPolygon &lhs, const IPolygon &rhs) { return !operator<(lhs,rhs); }
  };

  class Ellipse : public IPolygon
  {
  protected:
    Point _center;
    double _a;
    double _b;

  public:
    Ellipse() {}
    Ellipse(const Point &center,
            const double &a,
            const double &b) : _center(center), _a(a), _b(b) {}
    virtual ~Ellipse() {}
    void AddVertex(const Point &point) {}
    double Perimeter() const
    {
      return 2 * M_PI * sqrt((_a * _a + _b * _b) / 2);
    }
  };

  class Circle : public Ellipse
  {
  public:
    Circle() {}
    Circle(const Point &center,
           const double &radius) : Ellipse(center, radius, radius) {}
    void AddVertex(const Point &point) {}
    virtual ~Circle() {}
  };

  class Triangle : public IPolygon
  {

  public:
    Triangle() {}
    Triangle(const Point &p1,
             const Point &p2,
             const Point &p3) {
               points.push_back(p1);
               points.push_back(p2);
               points.push_back(p3);
             }

    void AddVertex(const Point &point)
    {
      points.push_back(point);
    }
  };

  class TriangleEquilateral : public Triangle
  {
    double edge;

  public:
    TriangleEquilateral(const Point &p1,
                        const double &edge) : edge(edge) {
                          points.push_back(p1);
                        }
    double Perimeter() const
    {
      return 3 * edge;
    }
  };

  class Quadrilateral : public IPolygon
  {
  public:
    Quadrilateral() {}
    Quadrilateral(const Point &p1,
                  const Point &p2,
                  const Point &p3,
                  const Point &p4) {
                    points.push_back(p1);
                    points.push_back(p2);
                    points.push_back(p3);
                    points.push_back(p4);
                  }
    virtual ~Quadrilateral() {}
    void AddVertex(const Point &p)
    {
      points.push_back(p);
    }
  };

  class Rectangle : public Quadrilateral
  {
  public:
    Rectangle() {}
    Rectangle(const Point &p1,
              const Point &p2,
              const Point &p3,
              const Point &p4) : Quadrilateral(p1, p2, p3, p4) {}
    Rectangle(const Point &p1,
              const double &base,
              const double &height) : Rectangle(p1, p1 + Point{base, 0}, p1 + Point{base, height}, p1 + Point{0, height}) {}
    virtual ~Rectangle() {}
  };

  class Square : public Rectangle
  {
  public:
    Square() {}
    Square(const Point &p1,
           const Point &p2,
           const Point &p3,
           const Point &p4) : Rectangle(p1,p2,p3,p4){}
    Square(const Point &p1,
           const double &edge) : Rectangle(p1,edge,edge){}
    virtual ~Square() {}
  };
}

#endif // SHAPE_H
