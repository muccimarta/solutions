#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
}
Intersector2D1D::~Intersector2D1D()
{
}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d &planeNormal, const double &planeTranslation)
{
  this->planeNormalPointer = &planeNormal;
  this->planeTranslationPointer = &planeTranslation;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d &lineOrigin, const Vector3d &lineTangent)
{
  this->lineOriginPointer = &lineOrigin;
  this->lineTangentPointer = &lineTangent;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
  auto pointB = *lineOriginPointer + *lineTangentPointer;
  auto line = ParametrizedLine<double, 3>::Through(*lineOriginPointer, pointB);
  auto pNorm = planeNormalPointer->normalized();
  auto ptPointer = *planeTranslationPointer / planeNormalPointer->norm();

  auto plane = Hyperplane<double, 3>(pNorm, ptPointer);
  if (std::fabs(pNorm.dot(*lineOriginPointer) + ptPointer) < toleranceParallelism && std::fabs(pNorm.dot(pointB) + ptPointer) < toleranceParallelism)
  {
    intersectionType = Coplanar;
    return false;
  }
  else if (std::fabs(pNorm.dot(*lineOriginPointer) - pNorm.dot(pointB)) < toleranceParallelism)
  {
    intersectionType = NoInteresection;
    return false;
  }
  else
  {
    intersectionType = PointIntersection;
    intersectionPoint = line.intersectionPoint(plane);
    return true;
  }
}
