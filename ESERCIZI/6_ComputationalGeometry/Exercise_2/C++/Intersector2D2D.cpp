#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
}

Intersector2D2D::~Intersector2D2D()
{
}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d &planeNormal, const double &planeTranslation)
{
  this->planeNormal1 = planeNormal;
  this->planeTranslation1 = planeTranslation;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d &planeNormal, const double &planeTranslation)
{
  this->planeNormal2 = planeNormal;
  this->planeTranslation2 = planeTranslation;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{

  auto pNorm1 = planeNormal1.normalized();
  auto pTransl1 = planeTranslation1 / planeNormal1.norm();
  auto pNorm2 = planeNormal2.normalized();
  auto pTransl2 = planeTranslation2 / planeNormal2.norm();
  auto plane1 = Hyperplane<double, 3>(pNorm1, pTransl1);
  auto plane2 = Hyperplane<double, 3>(pNorm1, pTransl1);
  if (std::fabs(std::fabs(pNorm1.dot(pNorm2)) - 1) < toleranceParallelism)
  {
    double zRef = -pTransl1/pNorm1.z();
    if(std::fabs(pNorm2.z() * zRef + pTransl1)<toleranceIntersection){
      intersectionType = Coplanar;
    }
    else{
      intersectionType = NoInteresection;
    }
    return false;
  }
  else
  {
    tangentLine = pNorm1.cross(pNorm2);
    double y = (pNorm2.x() * pTransl1 - pNorm1.x() * pTransl2) / (pNorm1.x() * pNorm2.y() - pNorm2.x()*pNorm1.y());
    double x = (pNorm1.y() * pTransl2 - pNorm2.y() * pTransl1) / (pNorm1.x() * pNorm2.y() - pNorm2.x() * pNorm1.y());
    pointLine = Vector3d(x,y,0);
    return true;
  }
}
